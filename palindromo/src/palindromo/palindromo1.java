package palindromo;
public class palindromo1 {
public boolean espalindromo(String cad){
   boolean valor=true;
     int i,dn;
     String cad2="";
    //quitamos los espacios
    for (int x=0; x < cad.length(); x++) {
        if (cad.charAt(x) != ' ')
            cad2 += cad.charAt(x);
    }
    //volvemos a asignar variables
    cad=cad2;    
    dn=cad.length();
    //comparamos cadenas
    for (i= 0 ;i < (cad.length()); i++){        
       if (cad.substring(i, i+1).equals(cad.substring(dn - 1, dn)) == false ) {
           //si una sola letra no corresponde no es un palindromo por tanto
           //sale del ciclo con valor false
            valor=false;
            break;
       }
       dn--;
    }
    return valor;
}
}