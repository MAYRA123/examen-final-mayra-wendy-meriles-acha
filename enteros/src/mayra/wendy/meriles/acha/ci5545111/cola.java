package mayra.wendy.meriles.acha.ci5545111;

/**
 *
 * @author pc
 */
public class cola {
    private Object dato;
    private cola enlace;
    public cola(Object dato)
    {
        this.dato = dato;
        this.enlace = null;
    }
    public cola(Integer dato)
    {
        this.dato = dato;
        this.enlace = null;
    }
    public cola(float dato)
    {
        this.dato = dato;
        this.enlace = null;
    }
    public cola(String dato)
    {
        this.dato = dato;
        this.enlace = null;
    }
    public cola(boolean dato)
    {
        this.dato = dato;
        this.enlace = null;
    }

    public Object getDato() {
        return dato;
    }

    public void setDato(Object dato) {
        this.dato = dato;
    }

    public cola getEnlace() {
        return enlace;
    }

    public void setEnlace(cola enlace) {
        this.enlace = enlace;
    }
    
    public int compareTo(cola b){
        return this.dato.toString().compareTo(b.getDato().toString());
    }
    
    public static cola max(cola a,cola b){
        return ( a.compareTo(b)> 0? a:b );
    }
    public static cola min(cola a,cola b){
        return ( a.compareTo(b)< 0? a:b );
    }
}