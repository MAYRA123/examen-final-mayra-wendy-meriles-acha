package mayra.wendy.meriles.acha.ci5545111;
import java.util.Scanner;
public class Colas {
   static Scanner cin ;
    static list ls;
    public static void main(String[] args) {
        inicializacion();
        menu();
    }
    static void inicializacion()
    {
        cin = new Scanner(System.in);
        System.out.println("1.- dinamico");
        System.out.println("2.- limite");
        String op = cin.next();
        switch (op)
        {
            case "1": 
                ls = new list();break;
            case "2":
                System.out.println("ingrese el limte");
                int lim = cin.nextInt();
                ls = new list(lim);break;
        }   
    }
    static void mostrarMenu(){
        
        System.out.println("1.-añadir un nuevo elemento"); 
        System.out.println("2.-borrar un elemento de la estructura cola"); 
        System.out.println("3.-cambiar el limite de la cola"); 
        System.out.println("4.-mostrar el numero de elementos que existe en la estrucutra cola"); 
        System.out.println("5.-mostrar el elemeto minimo y maximo"); 
        System.out.println("6.-buscar un elemeto dentro de la estructura cola");
        System.out.println("7.-mostrar todos los elementos"); 
        System.out.println("8.-ordenar los elemetos de forma acendente o decendente");
        System.out.println("9.-mostrar todos los elemetos con su tipo de dato"); 
        System.out.println("10.-separar los tipos de datos en estructura de pilas que estos esten dentro de una estrucutra cola"); 
        System.out.println("11.-sacar todos los elemtos");
        System.out.println("12.-salir"); 
        System.out.print("Escoja una opcion: ");
    }
    static void menu()
    {
        boolean salir = true;
        int cont = 0;
        while(salir)
        {
            mostrarMenu();
            String op = cin.next();
            switch(op)
            {
                case "1": System.out.println("ingrese los datos");
                    if (!ls.lleno())
                        insertar(cin.next());
                    else
                       System.out.println("limite fue excedida");
                    break;
                case "2": 
                    System.out.println("ingrese el dato que desea eliminar");
                    ls.eliminar(cin.next());
                    //eliminar probar
                    break;
                case "3":
                    int aux = cin.nextInt();
                    if(aux>=ls.cantidad())
                        ls.setLimite(aux);
                    else
                        System.out.println("existen elemtos primero debe elinarlos");
                    break;
                case "4": 
                    System.out.println("la cantidad de elemetos es: "+ ls.cantidad());
                    break;
                case "5": 
                    ls.maxmin();
                    break;
                case "6": 
                    System.out.println("intruduzca lo que desea buscar");
                    ls.buscar(cin.next());
                    break;
                case "7":
                    ls.mostrar();
                    break;
                case "8": 
                    System.out.println("1.- acendente");
                    System.out.println("2.- decendente");
                    String orden = cin.next();
                    switch(orden){
                        case "1":
                            ls.ordenarAcendente();
                            break;
                        case "2":
                            ls.ordenarDecendente();
                            break;
                        default:
                            System.out.println("opcion incorrecta");
                    }
                    break;
                case "9": 
                    ls.mostrarConTipo();
                    break;
                case "10": 
                    cola c=ls.separarPorTiposEnPilas();
                    while(c!=null){
                        Pila p =(Pila) c.getDato();
                        boolean primero=true;
                        while(!p.vacio()){
                            if(primero){
                                Object o = p.deapilar();
                                String tipo=o.getClass().getName();
                                tipo = tipo.substring(tipo.lastIndexOf(".")+1);
                                System.out.println("Pila de "+tipo);
                                System.out.println(o.toString());
                                primero = false;
                            }
                            else
                                System.out.println(p.deapilar().toString());
                        }
                        c=c.getEnlace();
                        System.out.println();
                    }
                    break;
                case "11": 
                    while(!ls.vacia())
                        System.out.println("elemento "+ls.dequeue().toString());
                    break;
                case "N": salir = false;break;
            }
        }
   }
    public static void insertar(String dato){
        try{
            Integer x = Integer.parseInt(dato);
            ls.insertar(x);
        }
        catch(Exception e1){
            try{
                float x =Float.parseFloat(dato);
                ls.insertar(x);
            }
            catch(Exception e2){
                try{
                    if(dato.equals("true")||dato.equals("false")){
                        boolean x = Boolean.parseBoolean(dato);
                        ls.insertar(x);
                    }
                    else{
                        throw new RuntimeException("no es boolean");
                    }
                }
                catch(Exception e3){
                    ls.insertar(dato);
                }
            }
        }
    }
}




