package mayra.wendy.meriles.acha.ci5545111;

public class list {
  private cola raiz;
    private int cant = 0;
    private int limite;
    public list(){
        limite=-1;
    }
    public list(int limite){
        this.limite=limite;
    }
    
   public boolean vacia()
   {
      return raiz == null; 
   }
   public void insertar (Object dato)
   {
       cola cl = new cola(dato);
       inserta(cl);
   }
   public void insertar (Integer dato)
   {
       cola cl = new cola(dato);
       inserta(cl);
   }
   public void insertar (String dato)
   {
       
       cola cl = new cola(dato);
       inserta(cl);
   }
   public void insertar (float dato)
   {
       cola cl = new cola(dato);
       inserta(cl);
   }
   public void insertar (boolean dato)
   {
       cola cl = new cola(dato);
       inserta(cl);
   }
   private void inserta(cola cl)
   {
       if(vacia())
       {
           raiz = cl;
       }
       else
       {
           cl.setEnlace(raiz);
           raiz = cl;
       }
       cant++;
   }
   public void eliminar(Object dato)
   {
       cola aux = null;
       cola siguiente = raiz;
       int cont = 0;
       while(siguiente!=null)
       {
           if(siguiente.getDato().equals(dato))
           {
               cont++;
               break;
           }
           aux = siguiente;
           siguiente = siguiente.getEnlace();
       }
       if(cont > 0)
       {
           if(aux == null){
               raiz = raiz.getEnlace();
           }
           else{
               aux.setEnlace(siguiente.getEnlace());
               
           }
           System.out.println("el dato "+siguiente.getDato().toString()+ " fue eliminado");
           siguiente=null;
           cant--;
       }
       else
           System.out.println("no se a encontrado el elemento");
   }
   public void buscar(Object dato)
   {
       cola siguiente = raiz;
       int cont = 0;
       while(siguiente!=null)
       {
           if(siguiente.getDato().equals(dato))
           {
               System.out.println("existe el " + siguiente.getDato().toString()+" elemento en la cola");
               cont++;
           }
           siguiente = siguiente.getEnlace();
       }
       if(cont == 0)
       {
           System.out.println("no existe el elemeto en la cola");
       }
   }
   public int cantidad ()
   {
       return cant;
   }
   public void maxmin()
   {
       
        if(!vacia()){
            cola siguiente=raiz.getEnlace(),max=raiz,min=raiz;
            cola aux = raiz;
            while(siguiente!=null)
            {
                max=cola.max(max, siguiente);
                min=cola.min(min, siguiente);
                aux = siguiente;
                siguiente = siguiente.getEnlace();
            }
            System.out.println(min.getDato().toString()+" "+max.getDato().toString());
        }
        else
            System.out.println("La cola esta vacia");
   }
   public void mostrar()
   {
       cola siguiente = raiz;
       while(siguiente!=null)
       {
           System.out.print(siguiente.getDato().toString()+ " ");
           siguiente = siguiente.getEnlace();
       }
       System.out.println();
   }
   public void mostrarConTipo()
   {
       cola siguiente = raiz;
       while(siguiente!=null)
       {
           String tipo = siguiente.getDato().getClass().toString();
           tipo=tipo.substring(tipo.lastIndexOf(".")+1);
           System.out.print("("+siguiente.getDato().toString()+" - "+tipo+")"+ " ");
           siguiente = siguiente.getEnlace();
       }
       System.out.println();
   }
   public void ordenarAcendente()
   {
        if(!vacia()){
            cola siguiente=raiz;
            cola ordenada = null;
            //incercion
            while(siguiente!=null)
            {
                cola aux = siguiente;
                siguiente = siguiente.getEnlace();
                aux.setEnlace(null);
                if(ordenada == null)
                    ordenada = aux;
                else{
                    boolean insertado = false;
                    cola i,j;
                    for( i=ordenada,j=null;i!=null;i=i.getEnlace()){
                        if(cola.max(aux, i).equals(i)){
                            if(j==null){
                                aux.setEnlace(i);
                                ordenada=aux;
                                insertado = true;
                                break;
                            }
                            else{
                                j.setEnlace(aux);
                                aux.setEnlace(i);
                                insertado = true;
                                break;
                            }
                        }
                        j=i;
                    }
                    if(!insertado)
                        j.setEnlace(aux);
                }
            }
            raiz = ordenada;
        }
        else
            System.out.println("La cola esta vacia");
   }
   public void ordenarDecendente()
   {
        if(!vacia()){
            cola siguiente=raiz;
            cola ordenada = null;
            //incercion
            while(siguiente!=null)
            {
                cola aux = siguiente;
                siguiente = siguiente.getEnlace();
                aux.setEnlace(null);
                if(ordenada == null)
                    ordenada = aux;
                else{
                    boolean insertado = false;
                    cola i,j;
                    for( i=ordenada,j=null;i!=null;i=i.getEnlace()){
                        if(cola.min(aux, i).equals(i)){
                            if(j==null){
                                aux.setEnlace(i);
                                ordenada=aux;
                                insertado = true;
                                break;
                            }
                            else{
                                j.setEnlace(aux);
                                aux.setEnlace(i);
                                insertado = true;
                                break;
                            }
                        }
                        j=i;
                    }
                    if(!insertado)
                        j.setEnlace(aux);
                }
            }
            raiz = ordenada;
        }
        else
            System.out.println("La cola esta vacia");
   }
   public boolean lleno(){
       return cant==limite;
   }

    public int getLimite() {
        return limite;
    }

    public void setLimite(int limite) {
        this.limite = limite;
    }
    public Object dequeue(){
        cola aux = raiz;
        cant--;
        if(aux.getEnlace() == null){
            Object data = aux.getDato();
            raiz = null;
            return data;
        }
        else{
            while( aux .getEnlace().getEnlace()!=null )aux = aux.getEnlace();
            Object data = aux.getEnlace().getDato();
            aux.setEnlace(null);
            return data;
        }
    }
    public cola separarPorTiposEnPilas(){
        list l = new list();
        cola aux = null;
        while(!this.vacia()){
            Object data = this.dequeue();
            l.insertar(data);
            boolean apilado = false;
            if(aux==null){
                Pila p=nuevaPila(data);
                aux = new cola(p);
            }else{
                cola i;
                for(i = aux;i.getEnlace()!=null;i=i.getEnlace()){
                    Pila p =(Pila) i.getDato();
                    if(p.checkClass(data)){
                        p.apilar(data);
                        i.setDato(p);
                        apilado = true;
                        break;
                    }
                }
                if(!apilado){
                    Pila p =(Pila) i.getDato();
                    if(p.checkClass(data)){
                        p.apilar(data);
                        i.setDato(p);
                        apilado = true;
                    }
                    else{
                        Pila p2=nuevaPila(data);
                        i.setEnlace(new cola(p2));
                    }
                }
            }
        }
        
        while(!l.vacia()) this.insertar(l.dequeue());
        
        return aux;
    }
    private Pila nuevaPila(Object data){
        if(data.getClass()==Integer.class){
            Pila<Integer> p = new Pila<Integer>();
            p.apilar((Integer)data);
            return p;
        }
        if(data.getClass()==Float.class){
            Pila<Float> p = new Pila<Float>();
            p.apilar((Float)data);
            return p;
        }
        if(data.getClass()==Boolean.class){
            Pila<Boolean> p = new Pila<Boolean>();
            p.apilar((Boolean)data);
            return p;
        }
        if(data.getClass()==String.class){
            Pila<String> p = new Pila<String>();
            p.apilar((String)data);
            return p;
        } 
        return null;
    }
}
