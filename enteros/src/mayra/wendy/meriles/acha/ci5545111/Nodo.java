/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mayra.wendy.meriles.acha.ci5545111;

/**
 *
 * @author pc
 */
public class Nodo<E extends Object> {
    private E dato;
    private Nodo<E> enlace;
    public Nodo(E dato)
    {
        this.dato = dato;
        this.enlace = null;
    }
    public E getDato() {
        return dato;
    }

    public void setDato(E dato) {
        this.dato = dato;
    }

    public Nodo getEnlace() {
        return enlace;
    }

    public void setEnlace(Nodo enlace) {
        this.enlace = enlace;
    }
    
    public int compareTo(Nodo b){
        return this.dato.toString().compareTo(b.getDato().toString());
    }
   
    public boolean checkClass(Object o){
        return o.getClass()== dato.getClass();
    }
}
