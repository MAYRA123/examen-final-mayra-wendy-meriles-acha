package mayra.wendy.meriles.acha.ci5545111;
import java.io.Serializable;
public class Pila<E extends Object> implements Serializable{
    Nodo raiz;
    public Pila(){
        raiz = null;
    }
    
    public void apilar(E data){
        if(raiz == null){
            raiz = new Nodo(data);
            raiz.setEnlace(null);
        }else {
            Nodo aux = raiz;
            while(aux.getEnlace()!=null) aux=aux.getEnlace();
            aux.setEnlace(new Nodo(data));
        }
    }
    
    public E deapilar(){
        Nodo aux = raiz;
        if(aux.getEnlace() == null){
            Object data = aux.getDato();
            raiz = null;
            return (E)data;
        }
        else{
            while( aux .getEnlace().getEnlace()!=null )aux = aux.getEnlace();
            Object data = aux.getEnlace().getDato();
            aux.setEnlace(null);
            return (E)data;
        }
    }
    public boolean vacio(){
        return raiz == null;
    }
    public boolean checkClass(Object o){
        return raiz.checkClass(o);
    }
}
